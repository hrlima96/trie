require File.expand_path('no')

class Trie
  attr_reader :raiz

  def initialize
    @raiz = No.new("xxx")
    @lista = []
  end

  def insere(palavra)
    lista_palavra = palavra.downcase.split(//)

    x = @raiz.filhos

    for i in (0..lista_palavra.length - 1)

      if x[lista_palavra[i]]

        if i == lista_palavra.length - 1
          x[lista_palavra[i]].completa = true
        else
          x = x[lista_palavra[i]].filhos
        end

      else

        for j in (i..lista_palavra.length - 1)
          x[lista_palavra[j]] = No.new(lista_palavra[j])
          
          if j == lista_palavra.length - 1
            x[lista_palavra[j]].completa = true
          else
            x = x[lista_palavra[j]].filhos
          end

        end
        return

      end

    end

  end

  def busca(palavra)
    no = @raiz
    letras = palavra.downcase.split(//)

    if no.filhos[letras[0]]
      no = no.filhos[letras[0]]

      for i in (1..letras.length - 1)

        if no.filhos[letras[i]]
          no = no.filhos[letras[i]]
        else
          puts "Palavra nao encontrada"
          break
        end

      end

    else
      puts "Palavra nao encontrada"
    end

    #no.filhos.each do |key, value|
      p visita_filho(no, palavra)
    #end
    
  end

  def visita_filho(no, p)
    x = ""
    p += no.letra
    no.filhos.each do |key, value|
      if value.completa
        x += p + value.letra
      else
        p += visita_filho(value, p)
      end

      if value.completa
        x += "/"
      end
    end
    return x
  end

end
