class No
  attr_accessor :letra, :filhos, :completa

  def initialize(letra)
    @letra = letra
    @filhos = {}
    @completa = false
  end

  def to_s
    "***Letra: #{@letra} \nFilhos: #{@filhos} \nPalavra completa? #{@completa}***\n\n"
   end

end